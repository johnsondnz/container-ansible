#!/bin/bash

declare -a VERS=("2.7.0" "2.7.1" "2.7.2" "2.7.3" "2.7.4" "2.7.5" "2.7.6" "2.7.7" "2.7.8" "2.7.9" "2.7.10" "2.7.11" "2.7.12" "2.7.13" "2.7.14" "2.7.15" "2.7.16" "2.7.17" "2.7.18")

for i in ${VERS[@]}; do
  docker build -t johnsondnz/ansible:${i} --build-arg ANSIBLE_VERSION=${i} .
  docker push johnsondnz/ansible:${i}
  docker image rm johnsondnz/ansible:${i}
done

declare -a VERS=("2.8.0" "2.8.1" "2.8.2" "2.8.3" "2.8.4" "2.8.5" "2.8.6" "2.8.7" "2.8.8" "2.8.9" "2.8.10" "2.8.11" "2.8.12" "2.8.13" "2.8.14")

for i in ${VERS[@]}; do
  docker build -t johnsondnz/ansible:${i} --build-arg ANSIBLE_VERSION=${i} .
  docker push johnsondnz/ansible:${i}
  docker image rm johnsondnz/ansible:${i}
done

declare -a VERS=("2.9.0" "2.9.1" "2.9.2" "2.9.3" "2.9.4" "2.9.5" "2.9.6" "2.9.7" "2.9.8" "2.9.9" "2.9.10" "2.9.11" "2.9.12")

for i in ${VERS[@]}; do
  docker build -t johnsondnz/ansible:${i} --build-arg ANSIBLE_VERSION=${i} .
  docker push johnsondnz/ansible:${i}
  docker image rm johnsondnz/ansible:${i}
done
