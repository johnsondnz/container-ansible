FROM python:3.8 AS build
COPY requirements.txt /tmp

RUN set -x && \
    #echo "==> Install Terraform" && \
    #apk add --no-cache --update terraform && \
    echo "==> Update pip" && \
    pip3 install --no-cache-dir --upgrade pip && \
    echo "==> Checking for requirements.txt with contents" && \
    [ -s /tmp/requirements.txt ] && echo "==> Install more pip packages" && pip3 install --no-cache-dir -r /tmp/requirements.txt || echo "==> No additional packages to install" && \
    rm -rf /var/lib/apt/lists/* && \
    \
    echo "==> Adding hosts for convenience..."  && \
    mkdir -p /etc/ansible /ansible && \
    echo "[local]" >> /etc/ansible/hosts && \
    echo "localhost" >> /etc/ansible/host

RUN echo "==> Upgrade setuptools" && \
    pip3 install --no-cache-dir --upgrade setuptools

FROM python:3.8
ARG ANSIBLE_VERSION
LABEL maintainer="Donald Johnson <@johnsondnz>"

COPY --from=build /usr/local/lib/python3.8/site-packages /usr/local/lib/python3.8/site-packages

RUN echo "==> Install ansible" && \
    pip3 install --no-cache-dir ansible==${ANSIBLE_VERSION}
 
#ENV ANSIBLE_GATHERING smart
ENV ANSIBLE_HOST_KEY_CHECKING false
ENV ANSIBLE_RETRY_FILES_ENABLED false
ENV ANSIBLE_ROLES_PATH /ansible/playbooks/roles
ENV ANSIBLE_SSH_PIPELINING True
ENV PYTHONPATH /ansible/lib
ENV PATH /ansible/bin:$PATH
ENV ANSIBLE_LIBRARY /ansible/library
 
WORKDIR /ansible/playbooks
COPY VERSION .
COPY entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
